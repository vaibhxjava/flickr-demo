//
//  FlickrCoreDataHandler.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 02/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit
import CoreData

private let entityName = "SearchImageList"

class FlickrCoreDataHandler: NSObject {
    
    func saveImagesResponse(searchString:String?, pageNo:Int?, responseString:String?) {
        //self.getImages is for check duplicate entry in core data
        if self.getImages(search: searchString, page: pageNo).count == 0 {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {  return }
            let managedContext = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: entityName, in: managedContext)!
            let imgObject = NSManagedObject(entity: entity, insertInto: managedContext)
            let searchURL = "\(searchString ?? "")\(pageNo ?? 1)"
            imgObject.setValue(searchURL, forKeyPath: "url")
            imgObject.setValue(responseString, forKeyPath: "response")
            imgObject.setValue(pageNo, forKeyPath: "page")
            print("save data \(searchURL)")
            do {
              try managedContext.save()
            } catch let error as NSError {
              print("Could not save. \(error), \(error.userInfo)")
            }        }

    }
    
    
    func getImages(search:String?, page:Int?) -> [FlickerImage]{
        var imageArray:[FlickerImage]? = []
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {  return []}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest =  NSFetchRequest<NSManagedObject>(entityName: entityName)
        
        fetchRequest.fetchLimit = 1
        let searchString = "\(search ?? "")\(page ?? 1)"
        print("get data \(searchString)")
        let predicate = NSPredicate(format: "url == %@ ", searchString)
        fetchRequest.predicate = predicate
        
            do {
                let task = try managedContext.fetch(fetchRequest).first
                if let response = task?.value(forKey: "response") as? String {
                    if let responseDict = response.convertToDictionary() {

                        if let photos = responseDict["photos"] as? [String:Any] {
                            if let photo = photos["photo"] as? [[String:Any]] {
                                for item in photo {
                                    imageArray?.append(FlickerImage(fromDictionary: item))
                                }
                            }
                        }
                    }
                }
                return imageArray ?? []
            } catch let error {
                print(error.localizedDescription)
                return []
            }
    }
    
}

//
//  FlickrConstant.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 01/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import Foundation

let baseURL = "https://api.flickr.com/services/rest/?&method=flickr.photos.search&format=json&nojsoncallback=1&per_page=40&page=\(1)"
let apiString = "&api_key=415e2dbbcf245dc43437d58bf90fe22c"
let flickrBaseUrl = "https://www.flickr.com/photos/"


//
//  FlickrString.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 03/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit

extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = data(using: .utf8) {
            return try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        }
        return nil
    }
}

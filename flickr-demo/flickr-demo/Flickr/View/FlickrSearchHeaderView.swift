//
//  FlickrSearchHeaderView.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 01/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit

protocol FlickrSearchHeaderViewProtocol: NSObject {
    func searchText(text:String?)
    func openMenuPicker()
}

class FlickrSearchHeaderView: UIView {
    //MARK:- IBOutlets
    @IBOutlet var searchTextField: UITextField!
    
    //MARK:- variables
    weak  var delegate:FlickrSearchHeaderViewProtocol? = nil
    
    //MARK:- life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.searchTextField.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)

    }
    //MARK:- custom methods
   @objc func enterPressed() {
        self.endEditing(true)
        self.delegate?.searchText(text: searchTextField.text)
    }
    
    //MARK:- IBOutlet actions
    @IBAction func menuButtonTapped(_ sender: Any) {
        self.endEditing(true)
        self.delegate?.openMenuPicker()
    }
    @IBAction func searchText(_ sender: Any) {
        self.delegate?.searchText(text: searchTextField.text)

    }
    
    
}

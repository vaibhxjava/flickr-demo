//
//  FlickerImageModel.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 01/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import Foundation

class FlickerImage  : NSObject {

    var farm : Int?
    var url : String?
    var secret : String?
    var server : String?
    var title : String?

    init(fromDictionary dictionary: [String:Any]){
        farm = dictionary["farm"] as? Int
        secret = dictionary["secret"] as? String
        server = dictionary["server"] as? String
        title = dictionary["title"] as? String
        if let id = dictionary["id"] as? String {
            url = "https://farm\(farm ?? 0).staticflickr.com/\(server ?? "")/\(id)_\(secret ?? "")_m.jpg"
        }
    }

}

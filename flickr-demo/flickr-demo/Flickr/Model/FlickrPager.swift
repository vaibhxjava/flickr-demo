//
//  FlickrPager.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 03/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit

class FlickrPager: NSObject {
    var page : Int?
    var totalPage : Int?
    var loadMore: Bool = true
    var isLoading: Bool = false

    init(page:Int, totalPage:Int) {
        self.totalPage = totalPage
        self.page = page
        if page >= totalPage {
            self.loadMore = false
        }
    }
    
}

//
//  FlickrSearchViewController.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 01/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit
private let reuseIdentifier = "FlickrCollectionViewCell"

class FlickrSearchViewController: UIViewController {
    // MARK: - IBoutlets
    @IBOutlet var headerView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    // MARK: - Variables
    var viewModel = FlickrViewModel()
    var searchView = FlickrSearchHeaderView()

    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialUISetup()
    }
    
    // MARK: - custom methods
    //Initial UI setups
    func initialUISetup() {
        //add searchview
            searchView = Bundle.main.loadNibNamed("FlickrSearchHeaderView", owner: self, options: nil)?.first as! FlickrSearchHeaderView
            searchView.delegate = self
            self.headerView.addSubview(searchView)
            self.searchView.translatesAutoresizingMaskIntoConstraints = false
            searchView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor).isActive = true
            searchView.trailingAnchor.constraint(equalTo: headerView.trailingAnchor).isActive = true
            searchView.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
            searchView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
            self.view.layoutIfNeeded()
        // Register cell classes
        self.collectionView.register(UINib.init(nibName: "FlickrCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
    }

}

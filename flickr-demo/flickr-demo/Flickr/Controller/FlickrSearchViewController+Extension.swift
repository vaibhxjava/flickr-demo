//
//  FlickrSearchViewController+Extension.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 01/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit

//MARK:- CollectionView Delegate + Datasource methods
extension FlickrSearchViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.imageArray?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FlickrCollectionViewCell", for: indexPath) as? FlickrCollectionViewCell {
            if indexPath.item < self.viewModel.imageArray?.count ?? 0 {
                cell.image = self.viewModel.imageArray?[indexPath.item]
            }
            return cell
        }
    
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "FlickrPhotoDetailViewController") as? FlickrPhotoDetailViewController {
            self.navigationController?.pushViewController(vc, animated: true)
            vc.image = self.viewModel.imageArray?[indexPath.item]

        }
    }

}

//MARK:- CollectionView DelegateFlowLayout methods
extension FlickrSearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Int(UIScreen.main.bounds.width)/self.viewModel.selectedSizeRatio, height: Int(UIScreen.main.bounds.width)/self.viewModel.selectedSizeRatio)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.viewModel.imageArray?.count ?? 0-10 {

            self.viewModel.pager?.isLoading = true
            self.viewModel.loadMorePhotoByKeyword(searchString: searchView.searchTextField.text ?? "") { (status) in
                
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !(self.viewModel.pager?.isLoading ?? false)){
            self.viewModel.pager?.isLoading = true
            self.viewModel.loadMorePhotoByKeyword(searchString: searchView.searchTextField.text ?? "") { (status) in
                if status == true {
                    self.collectionView.reloadData()
                }
        }
    }
    
    }
}


//MARK:- HeaderView Protocol methods
extension FlickrSearchViewController: FlickrSearchHeaderViewProtocol {
    func searchText(text: String?) {
        self.viewModel.imageArray?.removeAll()
        self.collectionView.reloadData()
        self.viewModel.pager?.page = 1
        self.viewModel.getPhotoByKeyword(searchString: "Trending") { (status) in
            if status == true {
                self.collectionView.reloadData()
            }
        }
    }
    
    func openMenuPicker() {
        let alert = UIAlertController(title: "Flickr", message: "Select an option", preferredStyle: .actionSheet)
        let optionArray = [1,2,3,4]
        for item in optionArray {
          let action = UIAlertAction(title: "\(item)", style: .default) {
                UIAlertAction in
            self.viewModel.selectedSizeRatio = item
            self.collectionView.reloadData()
            }
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    

}

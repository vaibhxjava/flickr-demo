//
//  FlickrPhotoDetailViewController.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 01/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit

class FlickrPhotoDetailViewController: UIViewController {
    //MARK:- IBOutlet
    @IBOutlet var imageView: UIImageView!
    
    //MARK:- variables
    var image:FlickerImage?
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = image?.url {
            print(url)
            imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "download"))
        }
        self.title = image?.title
    }

}

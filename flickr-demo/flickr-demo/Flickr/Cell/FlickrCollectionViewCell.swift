//
//  FlickrCollectionViewCell.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 01/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit
import SDWebImage

class FlickrCollectionViewCell: UICollectionViewCell {
    //MARK:- IBOutlet
    @IBOutlet var imageView: UIImageView!
    
    //MARK:- variable
    var image:FlickerImage? {
        didSet {
            if let url = image?.url {
                imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "download"))
            }
        }
    }
}

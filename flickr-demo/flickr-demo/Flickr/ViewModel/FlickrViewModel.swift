//
//  FlickrViewModel.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 01/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit
import Alamofire
import ReachabilitySwift
import CoreData

class FlickrViewModel: NSObject {
    //MARK:- Variables
    var imageArray:[FlickerImage]? = []
    var selectedSizeRatio = 2
    let reachabilityManager = NetworkReachabilityManager()
    var pager:FlickrPager? = FlickrPager(page: 1, totalPage: 100)

    //MARK:- custom methods
    
    // get photo by keywordSearch
    func getPhotoByKeyword(searchString:String, completion:@escaping(_ success: Bool?)->()) {
        if FlickrConnectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
            self.getPhotoByKeywordOnline(searchString: searchString) { (status) in
                completion(true)
                return
            }
        }
        else {
            print("No! internet is not available.")
            self.getPhotoByKeywordOffline(searchString: searchString, page: 1) { (status) in
                self.pager?.isLoading = false
                self.pager?.loadMore = true
                completion(true)
                return
            }

        }
    }
    
    //load more photo by keywordSearch
    func loadMorePhotoByKeyword(searchString:String, completion:@escaping(_ success: Bool?)->())  {
        if self.pager?.loadMore == true {
            if FlickrConnectivity.isConnectedToInternet() {
                print("Yes! internet is available.")
                self.getPhotoByKeywordOnline(searchString: searchString) { (status) in
                    self.pager?.isLoading = false
                    completion(true)
                    return
                }
            }
            else {
                print("No! internet is not available.")
                self.pager?.page! += 1
                self.getPhotoByKeywordOffline(searchString: searchString, page: self.pager?.page ?? 0+1) { (status) in
                    self.pager?.isLoading = false
                    self.pager?.loadMore = true
                    completion(true)
                    return
                }

            }
        }
    }

    // load  photo by keywordSearch online mode
    private func getPhotoByKeywordOnline(searchString:String, completion:@escaping(_ success: Bool?)->()) {
        FlickrServiceAPI.shared.searchPhotoAPI(searchString: searchString, page: self.pager?.page ?? 0) { (responseData) in
            if responseData != nil {
                if let responseString = responseData?.toJsonString() {
                    self.saveResponseOffline(responseString: responseString, page: self.pager?.page, searchString: searchString)
                }
                if let photos = responseData?["photos"] as? [String:Any] {
                    if let totalPages = photos["pages"] as? Int, let page = photos["page"] as? Int {
                        self.pager = FlickrPager(page: page+1, totalPage: totalPages)

                    }
                    if let photo = photos["photo"] as? [[String:Any]] {
                        for item in photo {
                            self.imageArray?.append(FlickerImage(fromDictionary: item))
                        }
                    }
                }
                self.pager?.isLoading = false
                completion(true)
                return
            }
            else {
                completion(false)
                return
            }
        }
    }
    
    //get photo in offline mode
    private func getPhotoByKeywordOffline(searchString:String, page:Int? , completion:@escaping(_ success: Bool?)->()) {
        for image in FlickrCoreDataHandler().getImages(search: searchString, page: page) {
            self.imageArray?.append(image)
        }
        completion(true)
        return
    }

    //save photo for offline mode
    private func saveResponseOffline(responseString:String, page:Int?, searchString:String?) {
        FlickrCoreDataHandler().saveImagesResponse(searchString: searchString, pageNo: page, responseString: responseString)
    }
    
}

//
//  FlickrServiceAPI.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 01/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import UIKit
import Alamofire

class FlickrServiceAPI: NSObject {
    //MARK:- shared instance
    static let shared = FlickrServiceAPI()
    private override init() {}
    
    //MARK:- APIS
    //get current user
    func searchPhotoAPI(searchString:String?, page:Int, completion: @escaping(_ success: [String:Any]?)->()){
        let searchString = "&tags=\(searchString ?? "")&page="
        let url = baseURL + apiString + searchString + "\((page))"
        Alamofire.request(url, method: .get, parameters: nil,encoding:
            JSONEncoding.default, headers: nil).responseJSON {
         response in
         switch response.result {
                         case .success:
                          if let responseData = response.result.value as? [String:Any] {
                            // print(responseData.jsonString() ?? "")
                            completion(responseData)
                          }
                          break

                          case .failure(let error):
                           print(error)
                return
              }
         }
        }
    

}

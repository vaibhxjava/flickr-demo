//
//  FlickrConnectivity.swift
//  flickr-demo
//
//  Created by Vaibhav Singh on 02/05/20.
//  Copyright © 2020 Vaibhav Singh. All rights reserved.
//

import Foundation
import Alamofire

class FlickrConnectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
